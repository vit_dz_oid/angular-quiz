import { Answer } from "./answer";

export class Question {
  id: number;
  question: string;
  variantsOfAnswer: Answer[];
  answer?: number;
}
