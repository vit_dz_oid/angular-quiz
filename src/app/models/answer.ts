export class Answer {
  id: number;
  text: string;
  checked?: boolean;

  constructor(id?: number, text?: string) {
    this.id = id || 0;
    this.text = text || '';
  }
}
