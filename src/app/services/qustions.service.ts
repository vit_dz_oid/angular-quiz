import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Question } from "../models/question";
/* Set default
const Questions = [
  {
    id: 0,
    question: 'How many planets in solar system?',
    variantsOfAnswer: [
      {
        id: 0,
        text: '3'
      },
      {
        id: 1,
        text: '4'
      },
      {
        id: 2,
        text: '9'
      },
      {
        id: 3,
        text: '8'
      },
    ],
    answer: 2
  },
  {
    id: 1,
    question: 'The capital of United Kingdom is ...',
    variantsOfAnswer: [
      {
        id: 0,
        text: 'Paris'
      },
      {
        id: 1,
        text: 'London'
      },
      {
        id: 2,
        text: 'Minas Anor'
      },
    ],
    answer: 1
  },
  {
    id: 2,
    question: 'Who was the first human into outer space?',
    variantsOfAnswer: [
      {
        id: 0,
        text: 'Belka'
      },
      {
        id: 1,
        text: 'Strelka'
      },
      {
        id: 2,
        text: 'Yuri Gagarin'
      },
      {
        id: 3,
        text: 'George Bush'
      },
    ],
    answer: 2
  },
  {
    id: 3,
    question: 'The world is ...',
    variantsOfAnswer: [
      {
        id: 0,
        text: 'sphere'
      },
      {
        id: 1,
        text: 'flat'
      },
    ],
    answer: 0
  },
  {
    id: 4,
    question: 'Who let the dogs out?',
    variantsOfAnswer: [
      {
        id: 0,
        text: 'George Bush'
      },
      {
        id: 1,
        text: 'Vitaliy'
      },
      {
        id: 2,
        text: 'Yuval'
      },
      {
        id: 3,
        text: 'It\'s not me!'
      },
    ],
    answer: 3
  }
];
localStorage.setItem(LOCATION_QUESTIONS, JSON.stringify(Questions));*/

const LOCATION_QUESTIONS = 'quizQuestions';

@Injectable()
export class QuestionsService {

  constructor() { }

  getQuestions(): Observable<Question[]> {
    const storageQuestions = localStorage.getItem(LOCATION_QUESTIONS);

    if(!storageQuestions) return of([]);

    try {
      const questions = JSON.parse(storageQuestions);

      // return of(questions.map(({answer, ...other}) => ({ ...other })));
      return of(questions);
    } catch (e) {
      return of([]);
    }
  }

  getAnswers(): Observable<Object>{
    // return this.getQuestions().flatMap(questions => {
    //   // find the answers and return
    // })
    const storageQuestions = localStorage.getItem(LOCATION_QUESTIONS);

    if(!storageQuestions) return of({});

    try {
      const questions = JSON.parse(storageQuestions);
      const arrOfAnswers = questions.map(({answer, id, ...other}) => ({ id, answer }));

      return of(arrOfAnswers.reduce((prev, next) => ({ ...prev, [next.id]: next.answer}), {}));
    } catch (e) {
      return of({});
    }
  }

  addQuestion(question: Question): boolean{
    const storageQuestions = localStorage.getItem(LOCATION_QUESTIONS) || '[]';

    try {
      const questions = JSON.parse(storageQuestions);
      questions.push(question);
      localStorage.setItem(LOCATION_QUESTIONS, JSON.stringify(questions));

      return true;
    } catch (e) {
      console.error('Error on add question: ', question, e);
      return false;
    }
  }

  updateQuestion(question: Question): boolean{
    const storageQuestions = localStorage.getItem(LOCATION_QUESTIONS) || '[]';

    try {
      const questions = JSON.parse(storageQuestions);

      localStorage.setItem(LOCATION_QUESTIONS, JSON.stringify(questions.map(item => {
        if (item.id === question.id) return question;

        return item;
      })));

      return true;
    } catch (e) {
      console.error('Error on add question: ', question, e);
      return false;
    }
  }

  removeQuestion(id: number): boolean{
    const storageQuestions = localStorage.getItem(LOCATION_QUESTIONS) || '[]';

    if(!storageQuestions) return false;

    try {
      const questions = JSON.parse(storageQuestions);

      localStorage.setItem(LOCATION_QUESTIONS, JSON.stringify(questions.filter(({ id: idQ }) => idQ !== id)));

      return true;
    } catch (e) {
      console.error('Error on add question: ', id, e);
      return false;
    }
  }
}
