import { Injectable } from '@angular/core';

@Injectable()
export class ModalService {
  private modals: any[] = [];

  add(modal: any) {
    // add modal to array of active modals
    this.modals.push(modal);
  }

  remove(modal: any) {
    // remove modal from array of active modals
    this.modals = this.modals.filter(m => m === modal);
  }

  open(id: string) {
    // open modal specified by id
    let modal = this.modals.find(modal => id === modal.id);
    modal.open();
  }

  close(id: string) {
    // close modal specified by id
    let modal = this.modals.find(modal => id === modal.id);
    modal.close();
  }
}
