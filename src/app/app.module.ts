import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ButtonsModule, ModalModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { QuizComponent } from './components/quiz/quiz.component';
import { StepControlsComponent } from './components/step-controls/step-controls.component';
import { QuestionComponent } from './components/question/question.component';
import { ResultComponent } from './components/result/result.component';

import { QuestionsService } from "./services/qustions.service";
import { AboutPageComponent } from './components/about-page/about-page.component';
import { AppRoutingModule } from './app-routing.module';
import { NavigationComponent } from './components/navigation/navigation.component';
import { ManageQuestionPageComponent } from './components/manage-question-page/manage-question-page.component';
import { EditableQuestionComponent } from './components/editable-question/editable-question.component';
import { QuestionCreatorEditorComponent } from './components/question-creator-editor/question-creator-editor.component';
import { ModalRemoveQuestionComponent } from './components/modal-remove-question/modal-remove-question.component';
import { ModalService } from "./services/modals.service";
import { ModalContainerComponent } from './components/modal-container/modal-container.component';
import { ModalProcessQuestionComponent } from './components/modal-process-question/modal-process-question.component';


@NgModule({
  declarations: [
    AppComponent,
    QuizComponent,
    StepControlsComponent,
    QuestionComponent,
    ResultComponent,
    AboutPageComponent,
    NavigationComponent,
    ManageQuestionPageComponent,
    EditableQuestionComponent,
    QuestionCreatorEditorComponent,
    ModalRemoveQuestionComponent,
    ModalContainerComponent,
    ModalProcessQuestionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule.forRoot(),
    ModalModule.forRoot(),
    AppRoutingModule
  ],
  providers: [QuestionsService, ModalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
