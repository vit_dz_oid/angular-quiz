import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuizComponent } from "./components/quiz/quiz.component";
import { AboutPageComponent } from './components/about-page/about-page.component';
import { ManageQuestionPageComponent } from "./components/manage-question-page/manage-question-page.component";

const routes: Routes = [
  { path: '', redirectTo: '/quiz', pathMatch: 'full' },
  { path: 'quiz', component: QuizComponent },
  { path: 'about', component: AboutPageComponent },
  { path: 'questions_editor', component: ManageQuestionPageComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
