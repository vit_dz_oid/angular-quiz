import {Component, Input, Output, EventEmitter } from '@angular/core';
import { Question } from '../../models/question';

@Component({
  selector: 'question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent{
  @Input() question: Question;
  @Output() onCheckAnswer = new EventEmitter<number>();

  constructor() { }

  handleCheckAnswer(id: number) {
    this.onCheckAnswer.emit(id);
  }
}
