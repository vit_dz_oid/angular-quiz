import {Component, OnInit, TemplateRef, Input, ViewChild} from '@angular/core';
import { ModalService } from "../../services/modals.service";
import {BsModalService} from "ngx-bootstrap/modal";
import {BsModalRef} from "ngx-bootstrap/modal/bs-modal-ref.service";

@Component({
  selector: 'modal-container',
  templateUrl: './modal-container.component.html',
  styleUrls: ['./modal-container.component.css']
})
export class ModalContainerComponent implements OnInit {
  @Input() id: string = null;
  @Input() title: string = null;
  @ViewChild('templateRemove') tempRef: TemplateRef<any>;
  modalRemoveRef: BsModalRef;

  constructor(private modalService: ModalService, private bsModalService: BsModalService) { }

  ngOnInit() {
    this.modalService.add(this);
  }

  // remove self from modal service when directive is destroyed
  ngOnDestroy(): void {
    this.modalService.remove(this);
  }

  open(): void {
    this.modalRemoveRef = this.bsModalService.show(this.tempRef)
  }

  close(): void {
    this.modalRemoveRef.hide();
  }
}
