import {Component, Input} from '@angular/core';
import {QuestionsService} from "../../services/qustions.service";
import {ModalService} from "../../services/modals.service";
import {Question} from "../../models/question";

@Component({
  selector: 'modal-remove-question',
  templateUrl: './modal-remove-question.component.html',
  styleUrls: ['./modal-remove-question.component.css']
})
export class ModalRemoveQuestionComponent {
  @Input() id: string = null;
  @Input() question: Question = null;

  constructor(private modalService: ModalService, private questionsService: QuestionsService) {}

  confirmRemove(): void {
    if (this.questionsService.removeQuestion(this.question.id)) {
      this.modalService.close(this.id);
    }
    else console.error('Caused some error');
  }

  declineRemove(): void {
    this.modalService.close(this.id);
  }
}
