import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRemoveQuestionComponent } from './modal-remove-question.component';

describe('ModalRemoveQuestionComponent', () => {
  let component: ModalRemoveQuestionComponent;
  let fixture: ComponentFixture<ModalRemoveQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRemoveQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRemoveQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
