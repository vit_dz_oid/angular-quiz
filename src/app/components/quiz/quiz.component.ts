import {Component, OnInit} from '@angular/core';
import { QuestionsService } from '../../services/qustions.service';
import {Question} from '../../models/question';

@Component({
  selector: 'quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})

export class QuizComponent implements OnInit {
  questions: Array<Question> = [];
  answers: Object;
  index: number = 0;
  points: number = 0;
  done: boolean = false;

  constructor(private questionsService: QuestionsService) {
  }

  ngOnInit() {
    this.getQuestions();
    this.getAnswers();
  }

  getQuestions(): void {
    this.questionsService.getQuestions()
      .subscribe(questions => this.questions = questions.map(item => ({ ...item })));
  }

  getAnswers(): void {
    this.questionsService.getAnswers()
      .subscribe(answers => this.answers = answers);
  }

  handleNext(): void {
    if (this.index < this.questions.length - 1) this.index += 1;
    else this.calculateResult();
  }

  handleBack(): void {
    if (this.index > 0) this.index -= 1;
  }

  handleCheckAnswer(idAnswer): void {
    this.questions[this.index].variantsOfAnswer =
      this.questions[this.index].variantsOfAnswer
        .map((answer) => ({...answer, checked: answer.id === idAnswer}));
  }

  calculateResult(): void {
    const rightAnswers = this.questions.filter((question) => {
      const answer = question.variantsOfAnswer.find(({ checked }) => checked);
      const checkedAnswerId = (answer && answer.id) || -1;
      const rightAnswerId = this.answers[question.id].toString();
      return checkedAnswerId === rightAnswerId;//question.answer;
    });

    this.points = (rightAnswers.length / this.questions.length) * 100;
    this.done = true;
  }
}
