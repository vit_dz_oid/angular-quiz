import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionCreatorEditorComponent } from './question-creator-editor.component';

describe('QuestionCreatorEditorComponent', () => {
  let component: QuestionCreatorEditorComponent;
  let fixture: ComponentFixture<QuestionCreatorEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionCreatorEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionCreatorEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
