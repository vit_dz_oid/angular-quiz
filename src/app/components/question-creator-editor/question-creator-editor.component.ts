import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {Question} from "../../models/question";
import {Answer} from "../../models/answer";
import {QuestionsService} from "../../services/qustions.service";

@Component({
  selector: 'question-creator-editor',
  templateUrl: './question-creator-editor.component.html',
  styleUrls: ['./question-creator-editor.component.css']
})
export class QuestionCreatorEditorComponent implements OnChanges {
  @Input() question: Question;
  @Output() onSaved = new EventEmitter<void>();
  @Output() onCancel = new EventEmitter<void>();

  questionForm: FormGroup;

  constructor(private fb: FormBuilder, private questionService: QuestionsService) {
    this.initForm();
  }

  ngOnChanges() {
    this.initForm(this.question);
  }

  initForm(questionObj?: Question): void {
    let question = (questionObj && questionObj.question) || '';
    let variantsOfAnswer: FormArray = this.fb.array([], this.validateMinCountOfAnswers(2));

    this.questionForm = this.fb.group({
      question: [question, Validators.required],
      variantsOfAnswer: variantsOfAnswer,
      answer: [(questionObj && questionObj.answer.toString()) || null, Validators.required]
    });

    if (questionObj) {
      questionObj.variantsOfAnswer.forEach((answer) => {
        this.addAnswer(answer);
      })
    } else {
      this.addAnswer();
    }
  }

  addAnswer(answerObj?: Answer): void {
    const answer: Answer = (answerObj || new Answer((this.questionForm.get('variantsOfAnswer') as FormArray).length));
    (<FormArray>this.questionForm.controls['variantsOfAnswer'])
      .push(
        (this.fb.group({
          text: [answer.text, this.validateEmpty()],
          id: answer.id.toString()
        })));
  }

  removeAnswer(index: number): void {
    if (index === +this.questionForm.get('answer').value) this.questionForm.patchValue({answer: null});
    (<FormArray>this.questionForm.controls['variantsOfAnswer']).removeAt(index);
  }

  onSubmit() {

    if (this.questionForm.valid) {
      const question = this.prepareQuestionToSave();

      let methodName = '';
      if (this.question) {
        methodName = 'updateQuestion';
      } else {
        methodName = 'addQuestion';
      }

      if (this.questionService[methodName](question)) {
        this.onSaved.emit();
        this.initForm();
      } else {
        console.error('Caused some error');
      }
    } else {
      console.log('trip form is not valid');
    }
  }

  handleClose(): void {
    this.onCancel.emit();
    this.initForm();
  }

  prepareQuestionToSave(): Question {
    const formModel = this.questionForm.value;
    // deep copy of form model lairs
    const answers: Answer[] = formModel.variantsOfAnswer;

    // return new `Hero` object containing a combination of original hero value(s)
    // and deep copies of changed form model values
    const saveQuestion: Question = {
      id: (this.question && this.question.id) || (new Date()).getTime(),
      question: formModel.question as string,
      variantsOfAnswer: answers,
      answer: +formModel.answer
    };
    return saveQuestion;
  }

  validateMinCountOfAnswers(minCount: number): ValidatorFn {
    return (controls: FormArray): { [key: string]: any } => {
      const forbidden = controls.length < minCount;
      return forbidden ? {'soFewAnswers': {value: 'Need more answers'}} : null;
    };
  }

  validateEmpty(): ValidatorFn {
    return (control: FormControl): { [key: string]: any } => {
      return !control.value ? {'empty': {value: 'Field can\'t be empty'}} : null;
    }
  }
}
