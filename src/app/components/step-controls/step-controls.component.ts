import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'step-controls',
  templateUrl: './step-controls.component.html',
  styleUrls: ['./step-controls.component.css']
})
export class StepControlsComponent {
  @Input() total: number = 0;
  @Input() currentIndex: number = 0;
  @Output() onBack = new EventEmitter<void>();
  @Output() onNext = new EventEmitter<void>();

  constructor() { }

  handleBack(): void{
    this.onBack.emit();
  }

  handleNext(): void{
    this.onNext.emit();
  }
}
