import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalProcessQuestionComponent } from './modal-process-question.component';

describe('ModalProcessQuestionComponent', () => {
  let component: ModalProcessQuestionComponent;
  let fixture: ComponentFixture<ModalProcessQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalProcessQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalProcessQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
