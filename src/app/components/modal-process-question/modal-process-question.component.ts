import {Component, Input} from '@angular/core';
import {Question} from "../../models/question";
import {ModalService} from "../../services/modals.service";

@Component({
  selector: 'modal-process-question',
  templateUrl: './modal-process-question.component.html',
  styleUrls: ['./modal-process-question.component.css']
})
export class ModalProcessQuestionComponent {
  @Input() id: string = null;
  @Input() question: Question = null;

  constructor(private modalService: ModalService) {}

  handleClose(): void {
    this.modalService.close(this.id);
  }
}
