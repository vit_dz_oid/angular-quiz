import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Question} from "../../models/question";
import {QuestionsService} from "../../services/qustions.service";
import { ModalService } from "../../services/modals.service";

@Component({
  selector: 'editable-question',
  templateUrl: './editable-question.component.html',
  styleUrls: ['./editable-question.component.css']
})
export class EditableQuestionComponent {
  @Input() question: Question = null;
  @Output() onUpdate = new EventEmitter<void>();

  constructor(private questionsService: QuestionsService,
              private modalService: ModalService) { }

  editQuestion(idEl): void {
    this.modalService.open(idEl);
  }

  deleteQuestion(idEl): void {
    this.modalService.open(idEl);
  }
}
