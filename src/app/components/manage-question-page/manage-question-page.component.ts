import { Component, OnInit } from '@angular/core';
import { ModalService } from "../../services/modals.service";
import {QuestionsService} from "../../services/qustions.service";
import {Question} from "../../models/question";

@Component({
  selector: 'manage-question-page',
  templateUrl: './manage-question-page.component.html',
  styleUrls: ['./manage-question-page.component.css']
})
export class ManageQuestionPageComponent implements OnInit {
  questions: Question[] = [];

  constructor(private questionsService: QuestionsService, private modalService: ModalService) { }

  ngOnInit() {
    this.getQuestions();
  }

  getQuestions(): void {
    this.questionsService.getQuestions()
      .subscribe(questions => this.questions = questions.map(item => ({ ...item })));
  }

  addQuestion(idEl): void {
    this.modalService.open(idEl);
  }

  handleUpdate(): void {
   this.getQuestions();
  }
}
