import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageQuestionPageComponent } from './manage-question-page.component';

describe('ManageQuestionPageComponent', () => {
  let component: ManageQuestionPageComponent;
  let fixture: ComponentFixture<ManageQuestionPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageQuestionPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageQuestionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
